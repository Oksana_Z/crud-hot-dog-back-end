const mongoose = require('mongoose');

const SandwichSchema = mongoose.Schema({
    title: String,
    ingredients: String
}, {
    timestamps: true
});

module.exports = mongoose.model('Sandwich', SandwichSchema);
