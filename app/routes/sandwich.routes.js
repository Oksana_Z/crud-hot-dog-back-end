module.exports = (app) => {
    const sandwiches = require('../controllers/sandwich.controller.js');

    app.post('/sandwiches', sandwiches.create);

    app.get('/sandwiches', sandwiches.findAll);

    app.put('/sandwiches/:sandwichId', sandwiches.update);

    app.delete('/sandwiches/:sandwichId', sandwiches.delete);
}
