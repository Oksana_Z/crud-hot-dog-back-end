const Sandwich = require('../models/sandwich.model.js');

exports.create = (req, res) => {
  if(!req.body.ingredients) {
    return res.status(400).send({
      message: "Sandwiches' ingredients can not be empty"
    });
  }
  const sandwich = new Sandwich({
    title: req.body.title || "Untitled Sandwich",
    ingredients: req.body.ingredients
  });
  sandwich.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Sandwich."
        });
    });
};

exports.findAll = (req, res) => {
  Sandwich.find()
     .then(sandwiches => {
         res.send(sandwiches);
     }).catch(err => {
         res.status(500).send({
             message: err.message || "Some error occurred while retrieving sandwiches."
         });
     });
};

exports.update = (req, res) => {
  if(!req.body.ingredients) {
    return res.status(400).send({
      message: "Sandwiches' ingredients can not be empty"
    });
  }

  Sandwich.findByIdAndUpdate(req.params.sandwichId, {
    title: req.body.title || "Untitled Sandwich",
    ingredients: req.body.ingredients
  }, {new: true})
  .then(sandwich => {
    if(!sandwich) {
      return res.status(404).send({
        message: "Sandwich not found with id " + req.params.sandwichId
      });
    }
    res.send(sandwich);
  }).catch(err => {
    if(err.kind === 'ObjectId') {
      return res.status(404).send({
        message: "Sandwich not found with id " + req.params.sandwichId
      });
    }
    return res.status(500).send({
      message: "Error updating sandwich with id " + req.params.sandwichId
    });
  });
};

exports.delete = (req, res) => {
  Sandwich.findByIdAndRemove(req.params.sandwichId)
  .then(sandwich => {
    if(!sandwich) {
      return res.status(404).send({
        message: "Sandwich not found with id " + req.params.sandwichId
      });
    }
    res.send({message: "Sandwich deleted successfully!"});
  }).catch(err => {
    if(err.kind === 'ObjectId' || err.name === 'NotFound') {
      return res.status(404).send({
        message: "Sandwich not found with id " + req.params.sandwichId
      });
    }
    return res.status(500).send({
      message: "Could not delete sandwich with id " + req.params.sandwichId
    });
  });
};
